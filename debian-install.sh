
# TODO: Finish

# Compilers and Interpreters
# Installs: Common Lisp, Clang, Rust, etc.
echo "Installing languages."
sudo apt install clisp clang build-essential
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# Dev Environment
# Installs: Brew, npm, Yarn, Neovim, etc.
echo "Installing development tools."
sudo apt install npm yarn
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
brew install gcc
brew install --HEAD luajit
brew install --HEAD neovim
